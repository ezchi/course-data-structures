package binarytree

import "testing"

func BuildTree() *TreeT {
	t := &TreeT{
		Key: "Les",
		Left: &TreeT{
			Key:   "Cathy",
			Left:  &TreeT{Key: "Alex"},
			Right: &TreeT{Key: "Frank"}},
		Right: &TreeT{
			Key:  "Sam",
			Left: &TreeT{Key: "Nancy"},
			Right: &TreeT{
				Key:   "Violet",
				Left:  &TreeT{Key: "Tony"},
				Right: &TreeT{Key: "Wendy"}}}}

	return t
}

var ExpectedInOrderTraversalResult = "Alex Cathy Frank Les Nancy Sam Tony Violet Wendy "
var ExpectedPreOrderTraversalResult = "Les Cathy Alex Frank Sam Nancy Violet Tony Wendy "
var ExpectedPostOrderTraversalResult = "Alex Frank Cathy Nancy Tony Wendy Violet Sam Les "
var ExpectedLevelTraversalResult = "Les Cathy Sam Alex Frank Nancy Violet Tony Wendy "

func TestInOrderTraversal(t *testing.T) {
	tree := BuildTree()

	result := InOrderTraversal(tree)

	if result != ExpectedInOrderTraversalResult {
		t.Errorf("\nExpect: %s\nGot   : %s", ExpectedInOrderTraversalResult, result)
	}
}

func TestPreOrderTraversal(t *testing.T) {
	tree := BuildTree()

	result := PreOrderTraversal(tree)

	if result != ExpectedPreOrderTraversalResult {
		t.Errorf("\nExpect: %s\nGot   : %s", ExpectedPreOrderTraversalResult, result)
	}
}

func TestPostOrderTraversal(t *testing.T) {
	tree := BuildTree()

	result := PostOrderTraversal(tree)

	if result != ExpectedPostOrderTraversalResult {
		t.Errorf("\nExpect: %s\nGot   : %s", ExpectedPostOrderTraversalResult, result)
	}
}

func TestLevelTraversal(t *testing.T) {
	tree := BuildTree()

	result := LevelTraversal(tree)

	if result != ExpectedLevelTraversalResult {
		t.Errorf("\nExpect: %s\nGot   : %s", ExpectedLevelTraversalResult, result)
	}
}
