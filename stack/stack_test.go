package stack

import "testing"

func assertEmpty(s *StackT, t *testing.T) {
	if !s.Empty() {
		t.Errorf("Length of an empty stack should be 0, not %d", s.Len())
	}
}

func assertNotEmpty(s *StackT, t *testing.T) {
	if s.Empty() {
		t.Errorf("Length of an not empty stack should not be 0")
	}
}

func Test(t *testing.T) {
	s := New()

	assertEmpty(s, t)

	s.Push("abc")
	s.Push(10)

	assertNotEmpty(s, t)

	if s.Top() != 10 {
		t.Errorf("Top data should be 10")
	}

	if s.Pop() != 10 {
		t.Errorf("Pop data should be 10")
	}

	assertNotEmpty(s, t)

	if s.Pop() != "abc" {
		t.Errorf("Pop data should be \"abc\"")
	}

	assertEmpty(s, t)
}
