package binarytree

import (
	"fmt"
	"math"

	"gitlab.com/ezchi/course-data-structures/queue"
)

// TreeT defines the binary tree struct
type TreeT struct {
	Key   interface{}
	Left  *TreeT
	Right *TreeT
}

func (t TreeT) String() string {
	return fmt.Sprintf("%v ", t.Key)
}

// Height returns the high of tree
func (t TreeT) Height() float64 {
	if t.Key == nil {
		return 0.0
	}

	return 1.0 + math.Max(t.Left.Height(), t.Right.Height())
}

// Size returns the number of number elements in the tree
func (t TreeT) Size() int {
	if t.Key == nil {
		return 0
	}

	return 1 + t.Left.Size() + t.Right.Size()
}

func InOrderTraversal(t *TreeT) string {
	var s string

	if t.Left != nil {
		s = InOrderTraversal(t.Left)
	}

	if t.Key != nil {
		s = fmt.Sprintf("%s%s", s, t)
	}

	if t.Right != nil {
		s = fmt.Sprintf("%s%s", s, InOrderTraversal(t.Right))
	}
	return s
}

func PreOrderTraversal(t *TreeT) string {
	if t.Key == nil {
		return ""
	}

	s := fmt.Sprintf("%s", t)

	if t.Left != nil {
		s = fmt.Sprintf("%s%s", s, PreOrderTraversal(t.Left))
	}

	if t.Right != nil {
		s = fmt.Sprintf("%s%s", s, PreOrderTraversal(t.Right))
	}

	return s
}

func PostOrderTraversal(t *TreeT) string {
	s := ""

	if t.Left != nil {
		s = PostOrderTraversal(t.Left)
	}

	if t.Right != nil {
		s = fmt.Sprintf("%s%s", s, PostOrderTraversal(t.Right))
	}

	if t.Key != nil {
		s = fmt.Sprintf("%s%s", s, t)
	}

	return s
}

func LevelTraversal(t *TreeT) string {
	if t.Key == nil {
		return ""
	}

	q := queue.New()
	q.Enqueue(t)

	s := ""

	for !q.Empty() {
		node := q.Dequeue().(*TreeT)

		s = fmt.Sprintf("%s%s", s, node)
		if node.Left != nil {
			q.Enqueue(node.Left)
		}

		if node.Right != nil {
			q.Enqueue(node.Right)
		}
	}

	return s
}

func New() *TreeT {
	return &TreeT{}
}
