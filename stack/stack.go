package stack

type node struct {
	data interface{}
	next *node
}

// StackT defines the struct type holds pointer of top stack node and size of stack
type StackT struct {
	head *node
	size int
}

// Top returns the most recently added key
func (s StackT) Top() interface{} {
	return s.head.data
}

// Empty return true is stack is empty
func (s StackT) Empty() bool {
	return s.size == 0
}

// Len return the number of items in stack
func (s StackT) Len() int {
	return s.size
}

// Push data into stack
func (s *StackT) Push(data interface{}) {
	s.head = &node{data, s.head}
	s.size++
}

// Pop top item from stack
func (s *StackT) Pop() interface{} {
	if s.Empty() {
		return nil
	}

	d := s.head.data
	s.head = s.head.next
	s.size--

	return d
}

// New creates a new stack
func New() *StackT {
	return &StackT{}
}
