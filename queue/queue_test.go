package queue

import "testing"

func assertEmpty(q *QueueT, t *testing.T) {
	if !q.Empty() {
		t.Errorf("Length of an empty queue should be 0, not %d", q.Len())
	}
}

func assertNotEmpty(q *QueueT, t *testing.T) {
	if q.Empty() {
		t.Errorf("Length of an not empty queue should not be 0")
	}
}

func Test(t *testing.T) {
	q := New()

	assertEmpty(q, t)

	q.Enqueue("abc")
	assertNotEmpty(q, t)
	if q.Peek() != "abc" {
		t.Errorf("First element of queue should be \"abc\"")
	}

	q.Enqueue(10)

	assertNotEmpty(q, t)

	if q.Dequeue() != "abc" {
		t.Errorf("Dequeue data should be \"abc\"")
	}

	assertNotEmpty(q, t)

	if q.Peek() != 10 {
		t.Errorf("First element in the queue should be 10")
	}

	if q.Dequeue() != 10 {
		t.Errorf("Dequeu data should be 10")
	}

	assertEmpty(q, t)
}
