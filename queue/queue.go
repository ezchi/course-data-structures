package queue

type node struct {
	data interface{}
	next *node
}

// QueueT defines the struct type holds pointer of head and tail of the queue
type QueueT struct {
	head *node
	tail *node
	size int
}

// Len returns the number of elements in the queue
func (q QueueT) Len() int {
	return q.size
}

// Enqueue adds key to collection
func (q *QueueT) Enqueue(key interface{}) {
	n := node{data: key}

	if q.Empty() {
		q.head = &n
		q.tail = &n
	} else {
		q.tail.next = &n
		q.tail = &n
	}

	q.size++
}

// Dequeue removes and returns least recently added key
func (q *QueueT) Dequeue() interface{} {
	if q.Empty() {
		return nil
	}

	d := q.head.data
	q.head = q.head.next
	q.size--

	return d
}

// Empty returns true if there's no element in the queue
func (q QueueT) Empty() bool {
	return q.size == 0
}

// Peek return the first element in the queue
func (q QueueT) Peek() interface{} {
	if q.Empty() {
		return nil
	}

	return q.head.data
}

// New creates a new queue
func New() *QueueT {
	return &QueueT{}
}
